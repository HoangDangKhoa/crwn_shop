@extends('layout_cart')
@section('content_cart')

<section id="cart_items">
		<div class="container">
        <div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL::to('/')}}">Home</a></li>
				  <li class="active">Thanh toán</li>
				</ol>
			</div>

			<div class="review-payment">
				<h2>Xem lại giỏ hàng</h2>
			</div>
            <div class="table-responsive cart_info">
				<table class="table table-condensed">
						<?php
							$content = Cart::content();
							

                        ?>
					<thead>
						<tr class="cart_menu">
							<td class="image">Item</td>
							<td class="description"></td>
							<td class="price">Price</td>
							<td class="quantity">Quantity</td>
							<td class="total">Total</td>
							<td></td>
						</tr>
					</thead>
					<tbody>
                       @foreach($content as $value_content)
						<tr>
							<td class="cart_product">
								<a href=""><img src="{{URL::to('public/upload/product/'.$value_content->options->image)}}" alt="" width="50" /></a>
							</td>
							<td class="cart_description">
								<h4><a href="">{{$value_content->name}}</a></h4>
								<p>Web ID: 1089772</p>
							</td>
							<td class="cart_price">
								<p>{{ number_format($value_content->price). " VNĐ" }}</p>
							</td>
							<td class="cart_quantity">
								<div class="cart_quantity_button">
									<form action="{{URL::to('/update-cart-quantity')}}" method="post">
									{{ csrf_field() }}
									<input class="cart_quantity_input" type="number" name="cart_quantity" value="{{$value_content->qty}}" autocomplete="off" size="2">
									<input type="hidden" value="{{$value_content->rowId}}" name="rowId_cart">
									<input type="submit" value="Cập nhật" name="update_qty" class="btn btn-primary btn-sm" style="margin-top: 0px;">
									</form>
								</div>
							</td>
							<td class="cart_total">
								<p class="cart_total_price">
									<?php 
										$sub_total = $value_content->price * $value_content->qty;
										echo number_format($sub_total). " VNĐ"
									?>

								</p>
							</td>
							<td class="cart_delete">
								<a class="cart_quantity_delete" href="{{URL::to('/delete-to-cart/'.$value_content->rowId)}}"><i class="fa fa-times"></i></a>
							</td>
						</tr>
						@endforeach
					
					</tbody>
				</table>
			</div>
            <form action="{{URL::to('/order-place')}}" method="post">
                {{ csrf_field() }}
                <div class="payment-options">
                        <span>
                            <label><input name="payment_option" value="1" type="checkbox"> Trả bằng ATM</label>
                        </span>
                        <span>
                            <label><input name="payment_option" value="2" type="checkbox"> Thanh toán bằng tiền mặt</label>
                        </span>
                        <span>
                            <label><input name="payment_option" value="3" type="checkbox"> Thanh toán thẻ ghi nợ</label>
                        </span>
						<input type="submit" value="Đặt hàng" name="send_order_place" class="btn btn-primary">
                </div>
            </form>
        </div>
        
	</section> <!--/#cart_items-->



@endsection	  