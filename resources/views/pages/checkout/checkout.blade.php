@extends('layout_cart')
@section('content_cart')

<section id="cart_items">
		<div class="container">
        <div class="breadcrumbs">
				<ol class="breadcrumb">
				  <li><a href="{{URL::to('/')}}">Home</a></li>
				  <li class="active">Check out</li>
				</ol>
			</div>

			<div class="register-req">
				<p>Please use Register And Checkout to easily get access to your order history, or use Checkout as Guest</p>
			</div><!--/register-req-->

			<div class="shopper-informations">
				<div class="row">
					
					<div class="col-sm-12 clearfix">
						<div class="bill-to">
							<p>Bill To</p>
							<div class="form-one">
								<form action="{{URL::to('/save-checkout-customer')}}">
								{{ csrf_field() }}
									<input type="text" name="shipping_email" placeholder="Email*">	
									<input type="text" name="shipping_name"  placeholder="Name *">
									<input type="text" name="shipping_address" placeholder="Address *">
									<input type="text" name="shipping_phone" placeholder="Phone">
									<textarea name="shipping_note" rows="16"></textarea>
									<input type="submit" value="Gửi" name="send_order" class="btn btn-primary">
								</form>
							</div>
							<!-- <div class="form-two">
								<form>
									<input type="text" placeholder="Zip / Postal Code *">
									<select>
										<option>-- Country --</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<select>
										<option>-- State / Province / Region --</option>
										<option>United States</option>
										<option>Bangladesh</option>
										<option>UK</option>
										<option>India</option>
										<option>Pakistan</option>
										<option>Ucrane</option>
										<option>Canada</option>
										<option>Dubai</option>
									</select>
									<input type="password" placeholder="Confirm password">
									<input type="text" placeholder="Phone *">
									<input type="text" placeholder="Mobile Phone">
									<input type="text" placeholder="Fax">
								</form>
							</div> -->
						</div>
					</div>
							
				</div>
			</div>
			<!-- <div class="review-payment">
				<h2>Review & Payment</h2>
			</div>

            <div class="payment-options">
					<span>
						<label><input type="checkbox"> Thanh toán khi nhận hàng</label>
					</span>
					<span>
						<label><input type="checkbox"> Check Payment</label>
					</span>
					<span>
						<label><input type="checkbox"> Paypal</label>
                    </span>
                    
			</div> -->
        </div>
        
	</section> <!--/#cart_items-->



@endsection	  