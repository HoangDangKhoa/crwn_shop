<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class HomeController extends Controller
{
    //
    public function index() {
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
        $all_product = DB::table('product')->where('product_status', '1')->limit(6)->orderby('product_id', 'desc')
        ->get();
        
        return view('pages.home')->with('cate_product', $cate_product)->with('brand_product', $brand_product)
        ->with('all_product', $all_product);
    }

    public function search(Request $rq) {
        $keywords = $rq->keywords_submit; 
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();

        $search_product = DB::table('product')->where('product_name', 'like', '%'.$keywords.'%')->get();
        
        return view('pages.product.search')->with('cate_product', $cate_product)
        ->with('brand_product', $brand_product)->with('search_product', $search_product);
    }
}
//30