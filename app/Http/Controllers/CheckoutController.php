<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
use Cart;
session_start();

class CheckoutController extends Controller
{
    public function login_checkout() {
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')
        ->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
        return view('pages.checkout.login_checkout')->with('cate_product', $cate_product)
        ->with('brand_product', $brand_product);
    }

    public function add_customer(Request $rq) {
        $data = array();
        $data['customer_name'] = $rq->customer_name;
        $data['customer_email'] = $rq->customer_email;
        $data['customer_password'] = md5($rq->customer_password);
        $data['customer_phone'] = $rq->customer_phone;

        $customer_id = DB::table('customers')->insertGetId($data);

        Session::put('customer_id', $customer_id);
        Session::put('customer_name', $rq->customer_name);

        return Redirect::to('/checkout');
    }

    public function checkout() {
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')
        ->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
        return view('pages.checkout.checkout')->with('cate_product', $cate_product)
        ->with('brand_product', $brand_product);
    }

    public function save_checkout_customer(Request $rq) {
        $data = array();
        $data['shipping_name'] = $rq->shipping_name;
        $data['shipping_phone'] = $rq->shipping_phone;
        $data['shipping_email'] = $rq->shipping_email;
        $data['shipping_note'] = $rq->shipping_note;
        $data['shipping_address'] = $rq->shipping_address;

        $shipping_id = DB::table('shipping')->insertGetId($data);

        Session::put('shipping_id', $shipping_id);
        return Redirect::to('/payment');
    }

    public function payment() {
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')
        ->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
        return view('pages.checkout.payment')->with('cate_product', $cate_product)
        ->with('brand_product', $brand_product);
    }

    public function logout_checkout() {
        Session::flush();
        return Redirect::to('/login-checkout');
    }

    public function login_customer(Request $rq) {
        $email = $rq->email_account;
        $password = md5($rq->password_account);
        $result = DB::table('customers')->where('customer_email', $email)
        ->where('customer_password', $password)->first();
       
       if($result) {
            Session::put('customer_id', $result->customer_id);
            return Redirect::to('/checkout');
       } else {
            return Redirect::to('/login-checkout');
       }
    }

    public function order_place(Request $rq) {
        // get paymnet method
        $data = array();
        $data['payment_method'] = $rq->payment_option;
        $data['payment_status'] = "Đang chờ xử lí";
        
        $paymnet_id = DB::table('payment')->insertGetId($data);

        // inser order
        $order_data = array();
        $order_data['customer_id'] = Session::get('customer_id');
        $order_data['shipping_id'] = Session::get('shipping_id');
        $order_data['payment_id'] = $paymnet_id;
        $order_data['order_total'] = Cart::total();
        $order_data['order_status'] = "Đang chờ xử lí";
        
        $order_id = DB::table('order')->insertGetId($order_data);

        // inser order_details
        $content = Cart::content();
        foreach($content as $v_content){
            $order_details_data = array();
            $order_details_data['order_id'] = $order_id;
            $order_details_data['product_id'] = $v_content->id;
            $order_details_data['product_name'] = $v_content->name;
            $order_details_data['product_price'] = $v_content->price;
            $order_details_data['product_sales_quantity'] = $v_content->qty;
            
            DB::table('order_details')->insert($order_details_data);
        }
        if($data['payment_method'] == 1){
            echo "Thanh toán thẻ ATM";
        } else if($data['payment_method'] == 2){
           Cart::destroy();
           $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')
           ->get();
           $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
           return view('pages.checkout.handcash')->with('cate_product', $cate_product)
           ->with('brand_product', $brand_product);
        } else {
            echo "Thẻ ghi nợ";
        }
        

        //return Redirect::to('/payment');
    }
}
