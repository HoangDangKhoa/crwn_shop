<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class AdminController extends Controller
{
    public function authLogin() {
        $admin_id = Session::get('admin_id');
        if($admin_id) {
            return Redirect::to('admin.dashboard');
        } else {
            return Redirect::to('admin')->send();
        }
    } 
    public function index() {
        return view('admin_login');
    }

    public function show_dashboard() {  
        $this->authLogin();
        return view('admin.dashboard');
    }

    public function dashboard(Request $rq) {
        $admin_email = $rq->admin_email;
        $admin_password = md5($rq->admin_password);

        $result = DB::table('admin')->where('admin_email', $admin_email)->where('admin_password', $admin_password)->first();
        if($result) {
            Session::put('admin_name', $result->admin_name);
            Session::put('admin_id', $result->admin_id);
            return Redirect::to('/dashboard');
        } else {
            Session::put('message', 'Mật khẩu hoặc tài khoản không chính xác!');
            return Redirect::to('/admin');
        }
        return view('admin.dashboard');
    }

    public function logout() {
        $this->authLogin();
        Session::put('admin_name',null);
        Session::put('admin_id', null);
        return Redirect::to('/admin');
    }
}
