<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
session_start();

class CategoryProduct extends Controller
{
    //
    public function authLogin() {
        $admin_id = Session::get('admin_id');
        if($admin_id) {
            return Redirect::to('admin.dashboard');
        } else {
            return Redirect::to('admin')->send();
        }
    } 
    public function all_category_product() {
        $this->authLogin();
        $all_category_product = DB::table('category_product')->get();
        $manager_category_product = view('admin.all_category_product')->with('all_category_product', $all_category_product);
        return view('admin_layout')->with('admin.all_category_product', $manager_category_product);
    }

    public function add_category_product() {
        $this->authLogin();
        return view('admin.add_category_prodcut');
    }

    public function save_category_product(Request $rq) {
        $this->authLogin();
        $data = array();
        $data['category_name'] = $rq->category_product_name;
        $data['category_desc'] = $rq->category_product_desc;
        $data['category_status'] = $rq->category_product_status;

        DB::table('category_product')->insert($data);
        Session::put('message', 'Thêm danh mục sản phẩm thành công');
        return Redirect::to('add-category-product');
        
    }

    public function unactive_category_product($category_product_id) {
        $this->authLogin();
        DB::table('category_product')->where('category_id', $category_product_id)->update(['category_status'=>0]);
        Session::put('message', 'Ẩn danh mục sản phẩm thành công');
        return Redirect::to('all-category-product');
    }

    public function active_category_product($category_product_id) {
        $this->authLogin();
        DB::table('category_product')->where('category_id', $category_product_id)->update(['category_status'=>1]);
        Session::put('message', 'Hiển thị danh mục sản phẩm thành công');
        return Redirect::to('all-category-product');
    }

    public function edit_category_product($category_product_id) {
        $this->authLogin();
        $edit_category_product = DB::table('category_product')->where('category_id', $category_product_id)->get();
        $manager_category_product = view('admin.edit_category_product')->with('edit_category_product', $edit_category_product);
        return view('admin_layout')->with('admin.edit_category_product', $manager_category_product);
    }

    public function update_category_product(Request $rq, $category_product_id) {
        $this->authLogin();
        $data = array();
        $data['category_name'] = $rq->category_product_name;
        $data['category_desc'] = $rq->category_product_desc;

        DB::table('category_product')->where('category_id', $category_product_id)->update($data);
        Session::put('message', 'Cập nhập danh mục sản phẩm thành công');
        return Redirect::to('all-category-product');
    }

    public function delete_category_product($category_product_id) {
        $this->authLogin();
        DB::table('category_product')->where('category_id', $category_product_id)->delete();
        Session::put('message', 'Xóa danh mục sản phẩm thành công');
        return Redirect::to('all-category-product');
    }

    // End Admin function page
    public function show_category_home($category_id) {
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
        $category_by_id = DB::table('product')
        ->join('category_product', 'category_product.category_id', '=', 'product.category_id')
        ->where('category_product.category_id','=', $category_id)->get();
        $category_name = DB::table('category_product')->where('category_product.category_id', $category_id)->limit(1)->get();
        return view('pages.category.show_category')->with('cate_product', $cate_product)
        ->with('brand_product', $brand_product)->with('category_by_id', $category_by_id)
        ->with('category_name', $category_name);
    }


    
}
// bài 13
