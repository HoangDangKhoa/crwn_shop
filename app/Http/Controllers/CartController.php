<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

use App\Http\Requests;
use Session;
use Illuminate\Support\Facades\Redirect;
use Cart;

class CartController extends Controller
{
    public function save_cart(Request $rq) {
        $product_id = $rq->productid_hidden;
        $quantity = $rq->quantity;
        $product_info = DB::table('product')->where('product_id', $product_id)->first();
       
        $data['id']= $product_info->product_id;
        $data['qty']= $quantity;
        $data['name']= $product_info->product_name;
        $data['price']= $product_info->product_price;
        $data['weight']= $product_info->product_price;
        $data['options']['image']= $product_info->product_image;
        Cart::add($data);
        // Cart::destroy();
        return Redirect::to('/show-cart');
       
    }

    public function show_cart() {
        $cate_product = DB::table('category_product')->where('category_status', '1')->orderby('category_id', 'desc')
        ->get();
        $brand_product = DB::table('brand')->where('brand_status', '1')->orderby('brand_id', 'desc')->get();
        return view('pages.cart.show_cart')->with('cate_product', $cate_product)
        ->with('brand_product', $brand_product);
    }

    public function delete_to_cart($rowId) {
        Cart::update($rowId,0);
        return Redirect::to('/show-cart');
    }

    public function update_cart_quantity(Request $rq) {
        $rowId= $rq->rowId_cart;
        $qty = $rq->cart_quantity;

        Cart::update($rowId,$qty);
        return Redirect::to('/show-cart');
    }
}
//37 2min